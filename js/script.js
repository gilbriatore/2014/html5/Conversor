function converterHTML(html){
  html = html.replace(/&amp;/g,'&amp;amp;');
  html = html.replace(/</g,'&amp;lt;');
  html = html.replace(/>/g,'&amp;gt;'); 
  html = html.replace(/\n/g,'<br />\n');
  html = html.replace(/\r/g,''); 
  html = html.replace(/\n|<br\s*\/?>/gi, "");
  return html;
}

function converter(){
  var htmlEntrada = document.getElementById("htmlEntrada");
  var htmlSaida = document.getElementById("htmlSaida");
  htmlSaida.value = converterHTML(htmlEntrada.value);
}